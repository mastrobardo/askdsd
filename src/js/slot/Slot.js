
import { Container } from 'pixi.js';
import Reel from './reel/Reel';

export default class Slot extends Container {

  constructor() {
    super();
    this.reels = [];
    this.reelAnimated = 0;
    this.canSpin = true;
  }

  build(config) {

    let reels = config.reels;
    let lng = reels.length;
    let i = 0;

    // this.mmask = new PIXI.Graphics();
    // this.mmask.beginFill(0xff0000);
    // this.mmask.lineStyle(2, 0xffffff);
    // this.mmask.arc(0, 0, 75 * lng, 0, Math.PI);

    for (i; i < lng; lng--) {
      reels[lng - 1].symbols.sort(function () {
        return (4 * Math.random() > 2) ? 1 : -1;
      });
      var reel = new Reel(reels[lng]);
      reel.buildCircle(lng, reels[lng - 1]);
      this.addChild(reel);
      reel.registerCallback(this.win);
      this.reels.push(reel);

    }

    // this.addChild(this.mmask);
    // this.mask = this.mmask;
  }

  win() {
    this.reelAnimated++;
    if (this.reelAnimated == 5) {
      TweenMax.to(this.scale, 1, { x: 1.5, y: 1.5 });
      TweenMax.to(this.position, 1, { x: 800, y: 500, ease: Power2.easeInOut, onComplete: ()=> {
          this.reset();
        } });
    }
  }

  reset() {
    TweenMax.to(this.scale, 1, { delay: 3,  x: 1, y: 1 });
    TweenMax.to(this.position, 1, { delay: 3, x: 400, y: window.innerHeight / 2,
      ease: Power2.easeInOut,
      onComplete: ()=> {
        this.canSpin = true;
        this.reelAnimated = 0;
        this.reels.forEach((ele) => {
          ele.reset();
        });
      }
    });
  }

  render() {
    this.reels.forEach((ele) => {
      ele.render();
    });
  }

  spin() {
    if (!this.canSpin) return;
    this.canSpin = false;
    this.reels.forEach((ele) => {
        ele.spin(this.canSpin);
      });
  }
}
