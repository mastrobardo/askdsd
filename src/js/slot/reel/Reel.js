
import { Container, Graphics, Sprite, TextureCache } from 'pixi.js';
import { TweenMax, Power2 } from 'gsap';

class Reel extends Container {

  constructor(config) {
    super();
    this.config = config;
    this.symbols = [];
    this.winCallback  = null;
    this.radius = 70;
    let radians = Math.PI / 180;
    this.angleMovement = 540 * radians;
    this.initAngleMovement = -60 * radians;
    this.symWidth = 40;
    this.spinAmount = 0;
  }

  buildCircle (id, config) {
    this.id = id;
    this.reel = new PIXI.Graphics();

    this.reel.lineStyle(0);
    let color = 0xFFFFFF * Math.random();
    this.reel.beginFill(color, .7);
    this.reel.drawCircle(0, 0, this.radius * this.id);
    this.reel.endFill();
    this.reelC = new PIXI.Sprite();
    this.reelC.addChild(this.reel);
    this.addChild(this.reelC);

    this.items = config.symbols.length;

    for (var i = 0; i < this.items; i++) {
      let sprite = new PIXI.Sprite();
      sprite.anchor.x = 0.5;
      sprite.anchor.y = 0.5;
      sprite.alpha = 0;
      let x = (this.radius * id) * Math.cos(2 * Math.PI * i / this.items);
      let y = (id * this.radius) * Math.sin(2 * Math.PI * i / this.items);
      let sym = new PIXI.Sprite.fromImage(`assets/${Math.floor(Math.random() * 2.99 + 1)}.png`);
      sprite.addChild(sym);
      this.reel.addChild(sprite);
      sprite.x = x;
      sprite.y = y;
      sym.width = this.symWidth;
      sym.height = this.symWidth;
      sym.x = -this.symWidth / 2;
      sym.y = -this.symWidth / 2;

      this.symbols.push(sprite);
      TweenMax.to(sprite, 1, { delay: .2 * this.id, alpha: 1, ease: Power2.easeOut });
    }
  }

  registerCallback(fun) {
    this.winCallback = fun;
  }

  spin() {
    TweenMax.to(this, 2, { spinAmount: this.initAngleMovement, ease: Power2.easeInOut });
    TweenMax.to(this, 4, { delay: 1, spinAmount: this.angleMovement, ease: Power2.easeOut,
      onComplete: ()=> { this.win();} });
  }

  reset() {
    for (let i = 0, lng = this.symbols.length; i < lng; ++i) {
      TweenMax.to(this.symbols[i], .1, { alpha: 1, ease: Power2.easeOut });
    }
  }

  win() {
    let it = 0;
    for (let i = 0, lng = this.symbols.length; i < lng; ++i) {
      if (i > 2) {
        TweenMax.to(this.symbols[i], 1, { delay: .1 * i, alpha: 0, ease: Power2.easeOut,
          onComplete: () => {
            if (it == 8) this.winCallback.apply(this.parent);
            it += 1;
          } });
      }
    }
  }

  render(time) {
    this.reelC.rotation = this.spinAmount * (this.id % 2 ? 1 : -1);
    this.symbols.forEach((ele) => {
      ele.rotation = -this.reelC.rotation;
    });
  }
}

export default Reel;
