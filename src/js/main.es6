import Slot from './slot/Slot';
import { Loader } from 'pixi.js';
import Howler from 'howler';

const stage = new PIXI.Stage(0x66FF99);
const renderer = PIXI.autoDetectRenderer(800, 800);
const slot = new Slot();

const button = new PIXI.Sprite.fromImage('assets/spin.png');
button.anchor.x = .5;
button.anchor.y = .5;
button.click = button.tap = slot.spin.bind(slot);
button.x = 400;
button.y = 600;
button.interactive = true;


function onAssetsLoaded(evt) {
  document.getElementsByClassName('center')[0].appendChild(renderer.view);
  window.onresize = function () {
    //poor man's resize
    renderer.view.style.maxHeight = window.innerHeight + 'px';
    slot.y = window.innerHeight / 2;
  };

  window.onresize();

  slot.build(evt.resources.slot.data);
  stage.addChild(slot);
  slot.x = 400;
  window.requestAnimationFrame(animate);
  slot.spin();

  let music = new Howler.Howl({
      src: ['assets/sounds/casino.mp3'],
      autoplay: false,
      loop: true
    });
  music.play();
  stage.addChild(button);
}

function animate(time) {
  window.requestAnimationFrame(animate);
  renderer.render(stage);
  slot.render(time);
}

let loader = new PIXI.loaders.Loader();

//in a real env i would use an atlas
loader.add('slot', 'config/slot.json');
loader.add('assets/1.png');
loader.add('assets/2.png');
loader.add('assets/3.png');
loader.add('assets/spin.png');
loader.once('complete', onAssetsLoaded);
loader.load();
