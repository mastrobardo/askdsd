import gulp from 'gulp';
import browserify from 'browserify';
import watchify from 'watchify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import gutil from 'gulp-util';

const files = {
  javascript: ['**/*.js', '!public/**/*', '!node_modules/**/*']
};

const opts = Object.assign({}, watchify.args, {
  entries: ['./src/js/main.es6'],
  debug: true
});
const b = watchify(browserify(opts));
b.transform(babelify.configure({experimental: true}));

function bundle() {
  return b.bundle()
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('./public'));
}

gulp.task('dev', bundle);
b.on('update', bundle);
b.on('log', gutil.log);

gulp.task('default', ['dev']);
